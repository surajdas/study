  
package Day19;

import java.awt.FlowLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;//step-1
import javax.swing.JTextField;
import javax.swing.JButton;
//button click notifiaction
import java.awt.event.ActionEvent;
import java.awt.event.AWTEventListener;
import java.awt.event.ActionListener;

/**
 *
 * @author SURAJ BANIYA
 */
public class MainWindow implements ActionListener{
      JFrame frame=new JFrame();
      JLabel lbl_num1;//step-2
      JLabel lbl_num2;
      JButton btn_close;
      JTextField txt_num1;
      JTextField txt_num2;
      JButton btn_add;
    public MainWindow() {
        frame=new JFrame();
        frame.setTitle("calculatoor");
        frame.setSize(300, 150);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new FlowLayout());
        
        lbl_num1=new JLabel();//step--3
        lbl_num2=new JLabel();
        lbl_num1.setText("Enter the first number\n");//step-4
         txt_num1=new JTextField();
         txt_num1.setColumns(10);
         frame.add(lbl_num1);//step-5
         frame.add(txt_num1);
        lbl_num2.setText("Enter the second number::");
        
      //  txt_num1=new JTextField();
       // txt_num1.setColumns(10);
        btn_add=new JButton();
        btn_close=new JButton();
        btn_close.addActionListener(this);
        btn_add.addActionListener(this);
        btn_close.setText("Close");
        btn_add.setText("Add");
        txt_num2=new JTextField();
        txt_num2.setColumns(10);
      //  frame.add(lbl_num1);//step-5
        frame.add(lbl_num2);
       // frame.add(txt_num1);
        frame.add(txt_num2);
        frame.add(btn_close);
        frame.add(btn_add);
        frame.setVisible(true);
        
    }
    @Override
    public void actionPerformed(ActionEvent ae)
    {
        if(ae.getSource()==btn_close)
        {
            System.out.println("Closing");
            System.exit(0);
        }
        else if(ae.getSource()==btn_add){
            System.out.println("Adding....");
            int num1,num2,num3;
            num1=Integer.parseInt(txt_num1.getText());
             num2=Integer.parseInt(txt_num2.getText());
             num3=num1+num2;
             System.out.println("Result::"+num3);
            
        }
    }
}
