 
package Day19;

 
public class UserData {
    
    String name;
    String address;
    String phoneNmuner;
     public  UserData(){
         this.name=" 0";
         this.address="0 ";
         this.phoneNmuner="0 ";
         
     }

    public UserData(String name, String address, String phoneNmuner) {
        this.name = name;
        this.address = address;
        this.phoneNmuner = phoneNmuner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNmuner() {
        return phoneNmuner;
    }

    public void setPhoneNmuner(String phoneNmuner) {
        this.phoneNmuner = phoneNmuner;
    }
    
    public String toString()
    {
        return(this.name+" "+this.address+","+this.phoneNmuner );
    }
     
     
     
    
}
